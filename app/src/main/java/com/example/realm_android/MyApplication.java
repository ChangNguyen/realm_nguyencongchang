package com.example.realm_android;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        Realm myRealm = Realm.getInstance(this);
        Realm myOtherRealm =
                Realm.getInstance(
                        new RealmConfiguration.Builder(this)
                                .name("myOtherRealm.realm")
                                .build()
                );
        super.onCreate();
    }
}
