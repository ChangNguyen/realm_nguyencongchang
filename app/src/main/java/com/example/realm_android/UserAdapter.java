package com.example.realm_android;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

class User_Adapter extends RecyclerView.Adapter<User_Adapter.UserViewHolder> {
    private List<User> list_user;

    public User_Adapter(List<User> list_user) {
        this.list_user = list_user;
    }

    @NonNull
    @NotNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item,parent,false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull User_Adapter.UserViewHolder holder, int position) {
        User user = list_user.get(position);
        if (user== null){
            return;
        }
        holder.tvName.setText(user.getName());
        holder.tvAddress.setText(user.getAge());
    }

    @Override
    public int getItemCount() {
        if (list_user!=null){
            return list_user.size();
        }
        return 0;
    }

    public class UserViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgAvatar;
        private TextView tvName,tvAddress;

        public UserViewHolder(@NonNull @org.jetbrains.annotations.NotNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.TV_NAME);
            tvAddress = itemView.findViewById(R.id.TV_AGE);
        }
    }
}
