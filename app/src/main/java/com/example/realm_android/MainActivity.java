package com.example.realm_android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    User_Adapter user_adapter;
    List<User> usersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.RCV_DATA);
        usersList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        user_adapter = new User_Adapter(usersList);
        recyclerView.setAdapter(user_adapter);
        insertUser();
        updateUser();
        deleteUser();
        getListUser();
    }
    private void insertUser() {
        Realm realm = Realm.getInstance(this);
        realm.beginTransaction();
        User user = realm.createObject(User.class);
        user.setSessionId(1);
        user.setName("ABC");
        user.setAge(20);
        realm.commitTransaction();
    }

    private void updateUser() {
        Realm realm = Realm.getInstance(this);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                User user = realm.createObject(User.class);
                user.setName("Chiken");
                user.setAge(10);
                realm.copyToRealmOrUpdate(user);
            }
        });

    }

    private void deleteUser() {
        Realm realm = Realm.getInstance(this);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                User  user = realm.where(User.class).findFirst();;
                realm.deleteFromRealm(user);
            }
        });

    }

    private void  getListUser() {
        Realm realm = Realm.getInstance(this);
        RealmResults<User> user = realm.where(User.class).findAll();
    }
}

